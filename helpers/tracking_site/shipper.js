var ShipperClient, moment, request, titleCase;

({
    titleCase
} = require('change-case'));
request = require('request');
phantom = require('phantom');
rp = require('request-promise');
moment = require('moment-timezone');

ShipperClient = (function() {
    class ShipperClient {
        presentPostalCode(rawCode) {
            rawCode = rawCode != null ? rawCode.trim() : void 0;
            if (/^\d{9}$/.test(rawCode)) {
                return `${rawCode.slice(0, 5)}-${rawCode.slice(5)}`;
            } else {
                return rawCode;
            }
        }

        presentLocationString(location) {
            var field, i, len, newFields, ref;
            newFields = [];
            ref = (location != null ? location.split(',') : void 0) || [];
            for (i = 0, len = ref.length; i < len; i++) {
                field = ref[i];
                field = field.trim();
                if (field.length > 2) {
                    field = titleCase(field);
                }
                newFields.push(field);
            }
            return newFields.join(', ');
        }

        presentLocation({
            city,
            stateCode,
            countryCode,
            postalCode
        }) {
            var address;
            if (city != null ? city.length : void 0) {
                city = titleCase(city);
            }
            if (stateCode != null ? stateCode.length : void 0) {
                stateCode = stateCode.trim();
                if (stateCode.length > 3) {
                    stateCode = titleCase(stateCode);
                }
                if (city != null ? city.length : void 0) {
                    city = city.trim();
                    address = `${city}, ${stateCode}`;
                } else {
                    address = stateCode;
                }
            } else {
                address = city;
            }
            postalCode = this.presentPostalCode(postalCode);
            if (countryCode != null ? countryCode.length : void 0) {
                countryCode = countryCode.trim();
                if (countryCode.length > 3) {
                    countryCode = titleCase(countryCode);
                }
                if (address != null ? address.length : void 0) {
                    address = countryCode !== 'US' ? `${address}, ${countryCode}` : address;
                } else {
                    address = countryCode;
                }
            }
            if (postalCode != null ? postalCode.length : void 0) {
                address = address != null ? `${address} ${postalCode}` : postalCode;
            }
            return address;
        }

        presentResponse(response, requestData, cb) {
            return this.validateResponse(response, (err, shipment) => {
                var activities, adjustedEta, eta, presentedResponse, ref, status;
                if ((err != null) || (shipment == null)) {
                    return cb(err);
                }
                ({
                    activities,
                    status
                } = this.getActivitiesAndStatus(shipment));
                eta = this.getEta(shipment);
                if (eta != null) {
                    adjustedEta = moment(eta).utc().format().replace(/T00:00:00/, 'T23:59:59');
                }
                if (adjustedEta != null) {
                    adjustedEta = moment(adjustedEta).toDate();
                }
                presentedResponse = {
                    eta: adjustedEta,
                    service: this.getService(shipment),
                    weight: this.getWeight(shipment),
                    destination: this.getDestination(shipment),
                    activities: activities,
                    status: status
                };
                if ((requestData != null ? requestData.raw : void 0) != null) {
                    if (requestData.raw) {
                        presentedResponse.raw = response;
                    }
                } else {
                    if ((ref = this.options) != null ? ref.raw : void 0) {
                        presentedResponse.raw = response;
                    }
                }
                presentedResponse.request = requestData;
                return cb(null, presentedResponse);
            });
        }

        requestData(requestData, cb) {
            var opts, ref;
            opts = this.requestOptions(requestData);
            opts.timeout = (requestData != null ? requestData.timeout : void 0) || ((ref = this.options) != null ? ref.timeout : void 0);
            return request(opts, (err, response, body) => {
                if ((body == null) || (err != null)) {
                    console.log("Lỗi khi request : ", err);
                    return cb(err);
                }
                // console.log(body);
                if (response.statusCode !== 200 && response.statusCode !== 201) {
                    return cb(`response status ${response.statusCode}`);
                }
                // console.log(body);
                return this.presentResponse(body, requestData, cb);
            });
        }

        async requestDataWithJS(requestData, cb) {
            var opts, ref;
            opts = this.requestOptions(requestData);
            opts.timeout = (requestData != null ? requestData.timeout : void 0) || ((ref = this.options) != null ? ref.timeout : void 0);
            return request(opts, (err, response, body) => {
                if ((body == null) || (err != null)) {
                    console.log("Lỗi khi request : ", err);
                    return cb(err);
                }
                let $ = cheerio.load(body);
                console.log($.html());
                if (response.statusCode !== 200) {
                    return cb(`response status ${response.statusCode}`);
                }
                return this.presentResponse(body, requestData, cb);
            });
        }

        async requestDataWithPhantom(requestData, cb) {
            var opts, ref;
            opts = this.requestOptions(requestData);
            opts.timeout = (requestData != null ? requestData.timeout : void 0) || ((ref = this.options) != null ? ref.timeout : void 0);

            const instance = await phantom.create();
            const page = await instance.createPage();
            await page.on('onResourceRequested', function (requestData) {
                console.info('Requesting', requestData.url);
            });

            const status = await page.open(opts.uri);
            if (status !== 'success') {
                return cb('cannot open google.com');
            }
            const content = await page.property('content');

            // console.log("xxx"+content);
            await instance.exit();

            return this.presentResponse(content, requestData, cb);
        }
    };



    ShipperClient.STATUS_TYPES = {
        UNKNOWN: 0,
        SHIPPING: 1,
        EN_ROUTE: 2,
        OUT_FOR_DELIVERY: 3,
        DELIVERED: 4,
        DELAYED: 5
    };

    return ShipperClient;

})();

module.exports = {
    ShipperClient
};