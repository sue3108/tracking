var UpsClient = require('./tracking_site/ups').UpsClient,
    FedexClient = require('./tracking_site/fedex').FedexClient,
    UspsClient = require('./tracking_site/usps').UspsClient,
    LasershipClient = require('./tracking_site/lasership').LasershipClient,
    DhlClient = require('./tracking_site/dhl').DhlClient,
    OnTracClient = require('./tracking_site/ontrac').OnTracClient,
    UpsMiClient = require('./tracking_site/upsmi').UpsMiClient,
    AmazonClient = require('./tracking_site/amazon').AmazonClient,
    A1Client = require('./tracking_site/a1').A1Client,
    CanadaPostClient = require('./tracking_site/canada_post').CanadaPostClient,
    DhlGmClient = require('./tracking_site/dhlgm').DhlGmClient,
    PrestigeClient = require('./tracking_site/prestige').PrestigeClient;

module.exports = {
    UpsClient: UpsClient,
    FedexClient: FedexClient,
    UspsClient: UspsClient,
    LasershipClient: LasershipClient,
    DhlClient: DhlClient,
    OnTracClient: OnTracClient,
    UpsMiClient: UpsMiClient,
    AmazonClient: AmazonClient,
    A1Client: A1Client,
    CanadaPostClient: CanadaPostClient,
    DhlGmClient: DhlGmClient,
    PrestigeClient: PrestigeClient
};