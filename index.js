// start.js
const mongoose = require('mongoose');
const throng = require('throng');
require('dotenv').config({
  path: '.env'
});

mongoose.connect(process.env.MONGODB_URI || 'mongodb://root:asd123@ds018258.mlab.com:18258/trackingpackvn', {
  useNewUrlParser: true //add it
});
mongoose.Promise = require('bluebird');

mongoose.connection.on('error', (err) => {
  console.error(`🚫 Database Error 🚫  → ${err}`);
});

function start() {
  /* You should require your models here so you don't have to initialise them all the time in
  different controlers*/
  require('./models/keygen');
  const app = require('./app');
  app.set('port', process.env.PORT || 3000);
  var server_port = process.env.OPENSHIFT_NODEJS_PORT || 3000;
  var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
  const server = app.listen(server_port, server_ip_address, function () {
  // const server = app.listen(app.get('port'), () => {
    console.log(`Express running → PORT ${server.address().port}`);
  });
}

throng({
  workers: process.env.WEB_CONCURRENCY || 1,
}, start);