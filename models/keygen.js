var mongoose = require('mongoose');
var keygenSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true
    },
    keygen: {
        type: String,
        unique: true
    },
    isActive: {
        type: Boolean,
        default: true
    },
    expireDate: Date
}, {
    timestamps: true
});
mongoose.model('Keygen', keygenSchema);