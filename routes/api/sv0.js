const express = require('express');
const router = express.Router();
var rp = require('request-promise');
var moment = require('moment');

function parseDate(d, type) {
    var dateValid = moment(new Date(d)).isValid();
    if (!dateValid) return '';
    if (type == 'iso') {
        return moment(new Date(d)).toISOString();
    }
}

async function _getTracking(code, trackingNumber) {
    var url = `https://www.ordertracking.com/tracking.php?action=Tracking&tracknumber=${trackingNumber}&express=${code}`;
    var options = {
        uri: url,
        headers: {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
            'referer': ` https://www.ordertracking.com/result/${trackingNumber}`,
            'accept': 'application/json, text/plain, */*',
        }
    };
    return await rp(options).then((resp) => {
            return resp;
        })
        .catch(function (err) {
            return err;
        });
}

function reParseData(resp){
    var pattern = /(.*)/g;
    var content = resp.match(pattern);
    if (content && content.length === 2 && content[0]) {
        content = content[0].replace(/#/g, "");
        result = JSON.parse(content);
        if (result.code.trim() != '0') {
            if (result.code == '200') {
                var mk = makingDataOT(result);
                if (mk != false){
                    return mk;
                }
            }
        }
    }
    return false;
}

async function parseResponseData(resp, trackingNumber) {
    var pattern = /(.*)/g;
    var content = resp.match(pattern);
    if (content && content.length === 2 && content[0]) {
        content = content[0].replace(/#/g, "");
        result = JSON.parse(content);
        if (result.code.trim() != '0') {
            if (result.code == '200') {
                var mk = makingDataOT(result);
                if (mk != false)
                    return mk;
            }
            if (result.code == '4011') {
                if (result.hasOwnProperty('data')) {
                    if (result.data.hasOwnProperty('carrier_list')) {
                        carrier_list = result.data.carrier_list;
                        for (let i = 0; i < carrier_list.length; i++) {
                            var code = carrier_list[i].company_code;
                            var reContent = await _getTracking(code, trackingNumber);
                            if (reParseData(reContent)!=false){
                                return reParseData(reContent);
                            }
                        }
                    }
                }
            }
        }
    }
    return {
        "status": "Error",
        "data": "Cannot tracking"
    };
}

function makingDataOT(result) {
    if (result.data.data === undefined || result.data.meta.code == '429' || result.data.data.items[0].origin_info.trackinfo==null)
        return false;

    lang_Not_Found = 'Not Found';
    lang_Unkown = 'Unkown';
    lang_select_carrier = 'select carrier';
    lang_In_Transit = 'In Transit';
    lang_Delivered = 'Delivered';
    lang_Pick_Up = 'Pick Up';
    lang_Undelivered = 'Undelivered';
    lang_Exception = 'Exception';
    lang_Expired = 'Expired';
    lang_NoExpress = 'NoExpress';
    lang_This_number = 'This number can\'t be found at this moment. It\'s not available in the carrier\'s system yet. Please check back later.';
    lang_The_query = 'The query fails';
    lang_wait_one_second = 'wait one second and try again.';
    lang_Origin = 'Origin';
    lang_Destination = 'Destination';

    var data = defaultData();

    var number = result.data.data.items[0].tracking_number;
    var code = result.data.data.items[0].carrier_code;
    var lastEvent = result.data.data.items[0].lastEvent;
    var lastUpdateTime = result.data.data.items[0].lastUpdateTime;
    var original_country = result.data.data.items[0].original_country;
    var destination_country = result.data.data.items[0].destination_country;
    var status = result.data.data.items[0].status;
    var imgUrls = result.data.imgUrls;
    var expressName = result.data.expressName;
    var status_name = lang_Not_Found;

    data.code = number;
    data.destination = destination_country;
    data.from = original_country;
    data.eta = parseDate(lastUpdateTime, 'iso');
    data.service = expressName;
    data.status = status;

    if (result.data.data.items[0].origin_info) {
        var origin_info = result.data.data.items[0].origin_info;
        if (origin_info.hasOwnProperty('trackinfo')) {
            originTrackInfo = origin_info.trackinfo;
            if (originTrackInfo != null) {
                for (var i = 0; i < originTrackInfo.length; i++) {
                    var activity = {};
                    activity.timestamp = parseDate(originTrackInfo[i].Date, 'iso');
                    activity.location = originTrackInfo[i].Details;
                    activity.datetime = originTrackInfo[i].Date;
                    activity.details = originTrackInfo[i].StatusDescription;
                    data.activities.push(activity);
                }
            }
        }
    }
    return data;
}

function defaultData() {
    return {
        "status": 0,
        "activities": [],
        "weight": "",
        "service": "",
        "eta": "",
        "destination": "",
        "from": "",
        "code": ""
    }
}

// ordertracking.com
router.get('/', (req, res) => {
    var queries = req.query;
    if (!queries.hasOwnProperty('trackingNumber'))
        res.json({
            "status": "Error",
            "data": "Please enter trackingNumber"
        });

    if (!queries.trackingNumber)
        res.json({
            "status": "Error",
            "data": "Please enter trackingNumber"
        });

    var trackingNumber = queries.trackingNumber;
    var express = "";
    if (queries.hasOwnProperty('express')) express = queries.express;

    var url = `https://www.ordertracking.com/tracking.php?action=Tracking&tracknumber=${trackingNumber}&express=${express}`;

    var options = {
        uri: url,
        headers: {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
            'referer': ` https://www.ordertracking.com/result/${trackingNumber}`,
            'accept': 'application/json, text/plain, */*',
        },
        // json: true // Automatically parses the JSON string in the response
    };

    rp(options)
        .then(async (resp) => {
            res.json(await parseResponseData(resp, trackingNumber));
        })
        .catch(function (err) {
            console.log(err);
            res.json(err);
        });

});

module.exports = router;