const express = require('express');
const router = express.Router();
var rp = require('request-promise');
var moment = require('moment');

function defaultData() {
    return {
        "status": 0, // done
        "activities": [],
        "weight": "",
        "service": "", // done
        "eta": "",
        "destination": "", // done
        "from": "", // done
        "code": "" // done
    }
}


function makingData(result) {
    var data = defaultData();
    if (result.hasOwnProperty('to')) {
        var to = result.to;
        if (typeof to === 'object') {
            if (to.hasOwnProperty('countryName')) {
                data.destination = to.countryName;
            }
        }
    }

    if (result.hasOwnProperty('from')) {
        var from = result.from;
        if (typeof from === 'object') {
            if (from.hasOwnProperty('countryName')) {
                data.from = from.countryName;
            }
        }
    }

    if (result.hasOwnProperty('tracking_number')) {
        var tracking_number = result.tracking_number;
        data.code = tracking_number;
    }

    if (result.hasOwnProperty('status_text')) {
        var status_text = result.status_text;
        data.status = status_text;
    }

    if (result.hasOwnProperty('carrier')) {
        var carrier = result.carrier;
        if (typeof carrier === 'object') {
            if (carrier.hasOwnProperty('name')) {
                data.service = carrier.countryName;
            }
        }
    }

    if (result.hasOwnProperty('steps')) {
        var steps = result.steps;
        for (var i = 0; i < steps.length; i++) {
            var activity = {};
            activity.timestamp = steps[i].date + "T" + steps[i].time + '.000Z';
            activity.location = steps[i].location;
            activity.datetime = steps[i].date + "T" + steps[i].time;
            activity.details = steps[i].type;
            data.activities.push(activity);
        }
    }
    return data;
}

async function _getTracking(code, trackingNumber) {
    var url = `https://www.jumingo.com/en-us/trackings/get_data_by_tracking_number/${trackingNumber}/${code}`;
    var options = {
        uri: url,
        headers: {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
            'referer': `https://www.jumingo.com/en-us/tracking/auto/${trackingNumber}`,
            'accept': 'application/json, text/plain, */*',
        },
        json: true
    };
    return await rp(options).then((resp) => {
            return resp;
        })
        .catch(function (err) {
            return err;
        });
}

function reParseData(resp){
    if (typeof resp === 'object') {
        if (resp.hasOwnProperty('status')) {
            if (resp.status == 'success') {
                if (resp.hasOwnProperty('data')) {
                    var data = resp.data;
                    if (typeof data === 'object') {
                        if (data.hasOwnProperty('steps')) {
                            var steps = data.steps;
                            if (steps.length > 0) {
                                var mk = makingData(data);
                                if (mk != false)
                                    return mk;
                            }
                        }
                    }
                }
            }
        }
    }
    return false;
}

async function parseResponseData(resp, trackingNumber) {
    if (typeof resp === 'object') {
        if (resp.hasOwnProperty('status')) {
            if (resp.status == 'success') {
                if (resp.hasOwnProperty('data')) {
                    var data = resp.data;
                    if (typeof data === 'object') {
                        if (data.hasOwnProperty('steps')) {
                            var steps = data.steps;
                            if (steps.length > 0) {
                                var mk = makingData(data);
                                if (mk != false)
                                    return mk;
                            }
                            if (steps.length <= 0) {
                                if (resp.hasOwnProperty('carrierList')) {
                                    var carrierList = resp.carrierList;
                                    if (carrierList != null){
                                        for (let i = 0; i < carrierList.length; i++) {
                                            var code = carrierList[i].code;
                                            var reContent = await _getTracking(code, trackingNumber);
                                            if (reParseData(reContent) != false) {
                                                return reParseData(reContent);
                                            }
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    return {
        "status": "Error",
        "data": "Cannot tracking"
    };
}
// Jumingo
router.get('/', (req, res) => {
    var queries = req.query;
    if (!queries.hasOwnProperty('trackingNumber'))
        res.json({
            "status": "Error",
            "data": "Please enter trackingNumber"
        });

    if (!queries.trackingNumber)
        res.json({
            "status": "Error",
            "data": "Please enter trackingNumber"
        });

    var trackingNumber = queries.trackingNumber;
    var express = "";
    if (queries.hasOwnProperty('express')) express = queries.express;

    var url = `https://www.jumingo.com/en-us/trackings/get_data_by_tracking_number/${trackingNumber}/${express}`;
    var options = {
        uri: url,
        headers: {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
            'referer': `https://www.jumingo.com/en-us/tracking/auto/${trackingNumber}`,
            'accept': 'application/json, text/plain, */*',
        },
        json: true // Automatically parses the JSON string in the response
    };

    rp(options)
        .then(async (resp) => {
            res.json(await parseResponseData(resp, trackingNumber));
        })
        .catch(function (err) {
            res.json(err);
        });

});

// router.get('/sv2', (req, res) => {
//     var queries = req.query;
//     if (!queries.hasOwnProperty('trackingNumber'))
//         res.json({
//             "status": "Error",
//             "data": "Please enter trackingNumber"
//         });

//     if (!queries.trackingNumber)
//         res.json({
//             "status": "Error",
//             "data": "Please enter trackingNumber"
//         });

//     var trackingNumber = queries.trackingNumber;

//     console.log(guessCarrier(trackingNumber))

//     var guessUrl = `http://packageassistant.com/v1/getcouriers/${trackingNumber}`;
//     var optionsGuess = {
//         uri: guessUrl,
//         headers: {
//             'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
//             'referer': `http://packageassistant.com/`,
//             'accept': 'application/json, text/plain, */*'
//         },
//         json: true
//     };

//     rp(optionsGuess)
//         .then(function (body) {
//             if (body.code === 200) {
//                 var html = '';
//                 if (body.couriers.length === 1) {
//                     var slug = body.couriers[0].slug;
//                     var url = `http://packageassistant.com/v1/tracking/${slug}/${trackingNumber}?id=${Math.random()}`;
//                     var options = {
//                         uri: url,
//                         headers: {
//                             'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
//                             'referer': `http://packageassistant.com/`,
//                             'accept': 'application/json, text/plain, */*'
//                         },
//                         json: true // Automatically parses the JSON string in the response
//                     };
//                     rp(options)
//                         .then(function (repos) {
//                             res.json(repos);
//                         })
//                         .catch(function (err) {
//                             res.json({
//                                 "status": "Error",
//                                 "data": err
//                             });
//                         });
//                 } else if (body.couriers.length) {
//                     body.couriers.forEach(courier => {
//                         var newURL = `http://packageassistant.com/v1/tracking/${courier.slug}/${trackingNumber}?id=${Math.random()}`;
//                         var options = {
//                             uri: newURL,
//                             headers: {
//                                 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
//                                 'referer': `http://packageassistant.com/`,
//                                 'accept': 'application/json, text/plain, */*'
//                             },
//                             json: true
//                         };
//                         rp(options).then(function (responses) {
//                                 if (!responses.hasOwnProperty('otherCouriers')) {
//                                     res.json(responses);
//                                 }
//                             })
//                             .catch(function (err) {
//                                 res.json({
//                                     "status": "Error",
//                                     "data": err
//                                 });
//                             });
//                     });
//                 } else {
//                     res.json({
//                         "status": "Error",
//                         "data": "Cannot get package infomation"
//                     });
//                 }
//             } else {
//                 res.json({
//                     "status": "Error",
//                     "data": "Cannot get package infomation"
//                 });
//             }
//         })
//         .catch(function (err) {
//             res.json({
//                 "status": "Error",
//                 "data": err
//             });
//         });
// });

module.exports = router;