/**
 * ./routes/api.js
 * This is where you'll set up any REST api endpoints you plan on using. (Shopify API)
 */
const express = require('express');
const router = express.Router();
const aio = require('./aio');
const auth = require('../middleware/keygen');

router.use('/aio',auth.isAuthorized, aio);

module.exports = router;