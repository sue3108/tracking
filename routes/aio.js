/**
 * ./routes/api.js
 * This is where you'll set up any REST api endpoints you plan on using. (Shopify API)
 */
const express = require('express');
const router = express.Router();
const sv0 = require('./api/sv0');
const sv1 = require('./api/sv1');
const auth = require('../middleware/keygen');

router.use('/sv0', sv0);
router.use('/sv1', sv1);

module.exports = router;