const express = require('express');
const mongoose = require('mongoose');

const router = express.Router();

const Keygen = mongoose.model('Keygen');

/* GET users listing. */
// router.get('/', (req, res, next) => {
//     const params = req.query;

//     Keygen.find({
//             keygen: params.shop
//         })
//         .sort({
//             updatedAt: -1
//         })
//         .exec(function (err, doc) {
//             if (err) {
//                 res.status(500).json(err);
//                 return;
//             };
//             res.status(200).json(doc);
//         });
// });

// Create Page 
router.post('/', async (req, res) => {
    try {
        console.log(req.body)
        var d = new Date();
        var newDate = d.setMonth(d.getMonth() + req.body.month);

        let keygen = new Keygen({
            keygen: req.body.keygen,
            expireDate: newDate
        });

        var result = await keygen.save();
        res.send(result);
    } catch (error) {
        console.log(error);
        res.status(500).send({
            'status': 'Error',
            'data': 'Không thể add key'
        });
    }
});


module.exports = router;