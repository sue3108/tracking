const mongoose = require('mongoose');
const Keygen = mongoose.model('Keygen');

module.exports.isAuthorized = function (req, res, next) {
    var queries = req.query;
    if (queries.keygen == undefined) {
        res.status(500).json({
            "status": "Error",
            "data": "Vui lòng nhập keygen"
        });
        return;
    }
    Keygen.findOne({
        keygen: queries.keygen
    }).exec(function (error, keygen) {
        if (error) {
            res.status(500).json({
                "status": "Error",
                "data": "Keygen không tồn tại"
            });
            return;
        }
        if (keygen == null){
            res.status(500).json({
                "status": "Error",
                "data": "Keygen không tồn tại"
            });
            return;
        }

        var d1 = new Date(keygen.expireDate);
        var d2 = new Date();
        if (d2 > d1) {
            res.status(500).json({
                "status": "Error",
                "data": "Keygen hết hạn"
            });
            return;
        } else {
            return next();
        }

    });
};